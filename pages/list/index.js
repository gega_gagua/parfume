import React, { useEffect } from "react";

const List = () => {
    useEffect(() => {
        fetch('https://qiraoba.ge/wp-json/getposts/get/?cat=&page=15').then(res => res.json()).then(data => {
            console.log(data);
        });
    }, [])


    return (
        <div className="main-content main-content-product left-sidebar" style={{ marginTop: '25px' }}>
            <div className="container">
                <div className="row">
                    <div className="content-area shop-grid-content no-banner col-lg-9 col-md-9 col-sm-12 col-xs-12">
                        <div className="site-main">
                            <div className="shop-top-control">
                                <form className="select-item select-form">
                                    <span className="title">Sort</span>
                                    <select title="sort" data-placeholder="12 Products/Page" className="chosen-select">
                                        <option value="1">12 Products/Page</option>
                                        <option value="2">9 Products/Page</option>
                                        <option value="3">10 Products/Page</option>
                                        <option value="4">8 Products/Page</option>
                                        <option value="5">6 Products/Page</option>
                                    </select>
                                </form>
                                <form className="filter-choice select-form">s
                                    <span className="title">Sort by</span>
                                    <select title="sort-by" data-placeholder="Price: Low to High" className="chosen-select">
                                        <option value="1">Price: Low to High</option>
                                        <option value="2">Sort by popularity</option>
                                        <option value="3">Sort by average rating</option>
                                        <option value="4">Sort by newness</option>
                                        <option value="5">Sort by price: low to high</option>
                                    </select>
                                </form>
                                <div className="grid-view-mode">
                                    <div className="inner">
                                        <a href="listproducts.html" className="modes-mode mode-list">
                                            <span></span>
                                            <span></span>
                                        </a>
                                        <a href="gridproducts.html" className="modes-mode mode-grid  active">
                                            <span></span>
                                            <span></span>
                                            <span></span>
                                            <span></span>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <ul className="row list-products auto-clear equal-container product-grid">
                                <li className="product-item  col-lg-4 col-md-6 col-sm-6 col-xs-6 col-ts-12 style-1">
                                    <div className="product-inner equal-element">
                                        <div className="product-top">
                                            <div className="flash">
                                                <span className="onnew">
                                                    <span className="text">
                                                        new
                                                    </span>
                                                </span>
                                            </div>
                                        </div>
                                        <div className="product-thumb">
                                            <div className="thumb-inner">
                                                <a href="#">
                                                    <img src="assets/images/product-item-1.jpg" alt="img" />
                                                </a>
                                                <div className="thumb-group">
                                                    <div className="yith-wcwl-add-to-wishlist">
                                                        <div className="yith-wcwl-add-button">
                                                            <a href="#">Add to Wishlist</a>
                                                        </div>
                                                    </div>
                                                    <a href="#" className="button quick-wiew-button">Quick View</a>
                                                    <div className="loop-form-add-to-cart">
                                                        <button className="single_add_to_cart_button button">Add to cart
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="product-info">
                                            <h5 className="product-name product_title">
                                                <a href="#">Aluminum Plant</a>
                                            </h5>
                                            <div className="group-info">
                                                <div className="stars-rating">
                                                    <div className="star-rating">
                                                        <span className="star-3"></span>
                                                    </div>
                                                    <div className="count-star">
                                                        (3)
                                                    </div>
                                                </div>
                                                <div className="price">
                                                    <del>
                                                        $65
                                                    </del>
                                                    <ins>
                                                        $45
                                                    </ins>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <li className="product-item product-type-variable col-lg-4 col-md-6 col-sm-6 col-xs-6 col-ts-12 style-1">
                                    <div className="product-inner equal-element">
                                        <div className="product-top">
                                        </div>
                                        <div className="product-thumb">
                                            <div className="thumb-inner">
                                                <a href="#">
                                                    <img src="assets/images/product-item-2.jpg" alt="img" />
                                                </a>
                                                <div className="thumb-group">
                                                    <div className="yith-wcwl-add-to-wishlist">
                                                        <div className="yith-wcwl-add-button">
                                                            <a href="#">Add to Wishlist</a>
                                                        </div>
                                                    </div>
                                                    <a href="#" className="button quick-wiew-button">Quick View</a>
                                                    <div className="loop-form-add-to-cart">
                                                        <button className="single_add_to_cart_button button">Add to cart
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="product-info">
                                            <h5 className="product-name product_title">
                                                <a href="#">Glorious Eau</a>
                                            </h5>
                                            <div className="group-info">
                                                <div className="stars-rating">
                                                    <div className="star-rating">
                                                        <span className="star-3"></span>
                                                    </div>
                                                    <div className="count-star">
                                                        (3)
                                                    </div>
                                                </div>
                                                <div className="price">
                                                    <del>
                                                        $65
                                                    </del>
                                                    <ins>
                                                        $45
                                                    </ins>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </li>

                            </ul>
                            <div className="pagination clearfix style3">
                                <div className="nav-link">
                                    <a href="#" className="page-numbers"><i className="icon fa fa-angle-left"
                                        aria-hidden="true"></i></a>
                                    <a href="#" className="page-numbers">1</a>
                                    <a href="#" className="page-numbers">2</a>
                                    <a href="#" className="page-numbers current">3</a>
                                    <a href="#" className="page-numbers"><i className="icon fa fa-angle-right"
                                        aria-hidden="true"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="sidebar col-lg-3 col-md-3 col-sm-12 col-xs-12">
                        <div className="wrapper-sidebar shop-sidebar">
                            <div className="widget woof_Widget">
                                <div className="widget widget-categories">
                                    <h3 className="widgettitle">Categories</h3>
                                    <ul className="list-categories">
                                        <li>
                                            <input type="checkbox" id="cb1" />
                                            <label for="cb1" className="label-text">
                                                New Arrivals
                                            </label>
                                        </li>
                                        <li>
                                            <input type="checkbox" id="cb2" />
                                            <label for="cb2" className="label-text">
                                                Dining
                                            </label>
                                        </li>
                                        <li>
                                            <input type="checkbox" id="cb3" />
                                            <label for="cb3" className="label-text">
                                                Desks
                                            </label>
                                        </li>
                                        <li>
                                            <input type="checkbox" id="cb4" />
                                            <label for="cb4" className="label-text">
                                                Accents
                                            </label>
                                        </li>
                                        <li>
                                            <input type="checkbox" id="cb5" />
                                            <label for="cb5" className="label-text">
                                                Accessories
                                            </label>
                                        </li>
                                        <li>
                                            <input type="checkbox" id="cb6" />
                                            <label for="cb6" className="label-text">
                                                Tables
                                            </label>
                                        </li>
                                    </ul>
                                </div>
                                <div className="widget widget_filter_price">
                                    <h4 className="widgettitle">
                                        Price
                                    </h4>
                                    <div className="price-slider-wrapper">
                                        <div data-label-reasult="Range:" data-min="0" data-max="3000" data-unit="$"
                                            className="slider-range-price " data-value-min="0" data-value-max="1000">
                                        </div>
                                        <div className="price-slider-amount">
                                            <span className="from">$45</span>
                                            <span className="to">$215</span>
                                        </div>
                                    </div>
                                </div>
                                <div className="widget widget-brand">
                                    <h3 className="widgettitle">Brand</h3>
                                    <ul className="list-brand">
                                        <li>
                                            <input id="cb7" type="checkbox" />
                                            <label for="cb7" className="label-text">New Arrivals</label>
                                        </li>
                                        <li>
                                            <input id="cb8" type="checkbox" />
                                            <label for="cb8" className="label-text">Dining</label>
                                        </li>
                                        <li>
                                            <input id="cb9" type="checkbox" />
                                            <label for="cb9" className="label-text">Desks</label>
                                        </li>
                                        <li>
                                            <input id="cb10" type="checkbox" />
                                            <label for="cb10" className="label-text">Accents</label>
                                        </li>
                                        <li>
                                            <input id="cb11" type="checkbox" />
                                            <label for="cb11" className="label-text">Accessories</label>
                                        </li>
                                        <li>
                                            <input id="cb12" type="checkbox" />
                                            <label for="cb12" className="label-text">Tables</label>
                                        </li>
                                    </ul>
                                </div>
                                <div className="widget widget-tags">
                                    <h3 className="widgettitle">
                                        Popular Tags
                                    </h3>
                                    <ul className="tagcloud">
                                        <li className="tag-cloud-link">
                                            <a href="#">Office</a>
                                        </li>
                                        <li className="tag-cloud-link">
                                            <a href="#">Accents</a>
                                        </li>
                                        <li className="tag-cloud-link">
                                            <a href="#">Flowering</a>
                                        </li>
                                        <li className="tag-cloud-link active">
                                            <a href="#">Accessories</a>
                                        </li>
                                        <li className="tag-cloud-link">
                                            <a href="#">Hot</a>
                                        </li>
                                        <li className="tag-cloud-link">
                                            <a href="#">Tables</a>
                                        </li>
                                        <li className="tag-cloud-link">
                                            <a href="#">Dining</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default List;