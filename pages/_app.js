import Layout from "../components/Layout";
import "../assets/css/bootstrap.min.css";
import "../assets/css/font-awesome.min.css";
import "../assets/css/owl.carousel.min.css";
import "../assets/css/animate.min.css";
import "../assets/css/jquery-ui.css";
import "../assets/css/slick.css";
import "../assets/css/chosen.min.css";
import "../assets/css/pe-icon-7-stroke.css";
import "../assets/css/magnific-popup.min.css";
import "../assets/css/lightbox.min.css";
import "../assets/js/fancybox/source/jquery.fancybox.css";
import "../assets/css/jquery.scrollbar.min.css";
import "../assets/css/mobile-menu.css";
import "../assets/fonts/flaticon/flaticon.css";
import "../assets/css/style.css";

function MyApp({ Component, pageProps }) {
  return (
    <Layout>
      <Component {...pageProps} />
    </Layout>
  );
}

export default MyApp;
